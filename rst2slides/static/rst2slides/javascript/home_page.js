
function changeImage() {
    var image = document.getElementById("theme_preview");
    var theme = document.getElementById("theme");
    var str = theme.value;
    image.src = "/static/rst2slides/images/"+str+".png"

}

function change_style(){
    var color, size;
    color = document.getElementById("text_color").value;
    size = document.getElementById("text_size").value;
    document.getElementById("text_area").style.color = color;
    document.getElementById("text_area").style.fontSize = size;
}


var app = angular.module('previewSlides', []);
app.controller('previewSlidesController', function($scope) {
	$scope.displayStatus = false;
	$scope.showPreview = function() {
		$scope.displayStatus = !$scope.displayStatus;
	}

});

function newPopup(url) {
	popupWindow = window.open(
		url,'popUpWindow','height=500,width=600,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
