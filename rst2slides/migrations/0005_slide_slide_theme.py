# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-11 14:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rst2slides', '0004_auto_20160511_2118'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='slide_theme',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
    ]
