from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect, get_object_or_404
from django.core.files.storage import default_storage
import os
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rst2slides.models import Slide
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone


# Create your views here.
def home_page(request):
    request.session['loginPage'] = ""
    request.session['loginPageSuccess'] = ""
    request.session['editSlide'] = ""
    return render(request, 'rst2slides/home_page.html')


def rst2html(request):
    submit = request.GET['submit']
    user = request.user
    theme = request.GET['theme']  # keep name of theme
    slide_name = request.GET['slide_name']
    if '_' in slide_name:
        request.session['home_page'] = "Slide name must not have _"
        return HttpResponseRedirect(reverse('rst2slides:home_page'))
    else:
        slide_name = slide_name.replace(" ","_")  # keep name of slide and change space between character to _

    if submit == 'submit':
        textFile = open('rst2slides/static/rst2slides/{}.rst'.format(slide_name), 'w')
        textFile.write(request.GET['myTextBox'])
        textFile.close()

        if theme == 'deck':
            os.system(
                'rst2html5 --deck-js --pretty-print-code --embed-content rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    slide_name,
                    slide_name))  # convert rst to html

        elif theme == 'clean':
            os.system(
                'rst2html5 rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    slide_name,
                    slide_name))  # convert rst to html

        elif theme == 'reveal':
            os.system(
                'rst2html5 --jquery --reveal-js --pretty-print-code rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    slide_name,
                    slide_name))  # convert rst to html


        elif theme == 'bootstrap':
            os.system(
                'rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    slide_name,
                    slide_name))  # convert rst to html

        htmlFile = open('rst2slides/templates/rst2slides/{}.html'.format(slide_name), 'r')
        html_content = ""
        for item in htmlFile:
            html_content = html_content + item  # keep all string in html file
        htmlFile.close()
        os.remove('rst2slides/static/rst2slides/{}.rst'.format(slide_name))  # remove rst file
        os.remove('rst2slides/templates/rst2slides/{}.html'.format(slide_name))  # remove html file
        return HttpResponse(html_content)
    else:

        textFile = open('rst2slides/static/rst2slides/{}.rst'.format(user.username), 'w')
        textFile.write(request.GET['myTextBox'])
        textFile.close()

        if theme == 'deck':
            os.system(
                'rst2html5 --deck-js --pretty-print-code --embed-content rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    user.username,
                    user.username,
                    ))  # convert rst to html

        elif theme == 'clean':
            os.system(
                'rst2html5 rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    user.username,
                    user.username,
                    ))  # convert rst to html

        elif theme == 'reveal':
            os.system(
                'rst2html5 --jquery --reveal-js --pretty-print-code rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    user.username,
                    user.username,
                    ))  # convert rst to html

        elif theme == 'bootstrap':
            os.system(
                'rst2html5 --bootstrap-css --pretty-print-code --jquery --embed-content rst2slides/static/rst2slides/{}.rst > rst2slides/templates/rst2slides/{}.html'.format(
                    user.username,
                    user.username,
                    ))  # convert rst to html

        htmlFile = open('rst2slides/templates/rst2slides/{}.html'.format(user.username), 'r')
        html_content = ""
        for item in htmlFile:
            html_content = html_content + item  # keep all string in html file
        htmlFile.close()
        os.remove('rst2slides/static/rst2slides/{}.rst'.format(user.username))  # remove rst file
        os.remove(
            'rst2slides/templates/rst2slides/{}.html'.format(user.username))  # remove html file

        if submit == 'save':
            list_slide = Slide.objects.filter(
                slide_username=user.username)  # current user can't create slide is have same name already but another user can create same name
            slide_name = request.GET['slide_name']

            if not list_slide:
                slide = Slide(slide_name=slide_name, slide_username=user.username, slide_rst=request.GET['myTextBox'],
                              slide_share_time=timezone.now(), slide_last_update=timezone.now(), slide_rating=0,
                              slide_html_content=html_content, slide_theme=theme)
                slide.save()
                return HttpResponseRedirect(reverse('rst2slides:userSlide'))
            else:
                for slide in list_slide:
                    if slide_name == slide.slide_name:
                        if (theme == slide.slide_theme) and (request.GET['myTextBox'] == slide.slide_rst):
                            request.session['home_page'] = "No Changes dectected"
                            return HttpResponseRedirect(reverse('rst2slides:home_page'))
                        else:
                            slide = Slide.objects.get(slide_name=slide_name)
                            slide.slide_rst = request.GET['myTextBox']
                            slide.slide_last_update = timezone.now()
                            slide.slide_html_content = html_content
                            slide.slide_theme = theme
                            slide.save()
                            return HttpResponseRedirect(reverse('rst2slides:userSlide'))

                slide = Slide(slide_name=slide_name, slide_username=user.username,
                              slide_rst=request.GET['myTextBox'],
                              slide_share_time=timezone.now(), slide_last_update=timezone.now(), slide_rating=0,
                              slide_html_content=html_content, slide_theme=theme)
                slide.save()
                return HttpResponseRedirect(reverse('rst2slides:userSlide'))

        elif submit == 'update':
            old_slide_name = request.session['old_slide_name']
            slide_name = request.GET['slide_name']
            slide = Slide.objects.get(slide_name=old_slide_name, slide_username=user.username)

            list_slide = Slide.objects.filter(
                slide_name=slide_name)  # current user can't create slide is have same name already but another user can create same name

            for checkSlide in list_slide:
                if (slide_name != old_slide_name) and (
                            slide_name == checkSlide.slide_name):  # if change new name and new name have already. it can't create new.
                    request.session['editSlide'] = "New name is has already"
                    return HttpResponseRedirect(reverse('rst2slides:editSlide', kwargs={'slide_name': old_slide_name.replace(" ","_"),
                                                                                        'slide_username': slide.slide_username}))

            if (theme == slide.slide_theme) and (request.GET['myTextBox'] == slide.slide_rst) and (
                        old_slide_name == slide_name):
                request.session['editSlide'] = "No Changes dectected"
                return HttpResponseRedirect(reverse('rst2slides:editSlide', kwargs={'slide_name': slide.slide_name.replace(" ","_"),
                                                                                    'slide_username': slide.slide_username}))
            else:
                slide.slide_rst = request.GET['myTextBox']
                slide.slide_name = slide_name  # change the slide name
                slide.slide_last_update = timezone.now()
                slide.slide_html_content = html_content
                slide.slide_theme = theme
                slide.save()
                return HttpResponseRedirect(reverse('rst2slides:userSlide'))

        elif submit == 'public':
            list_slide = Slide.objects.filter(
                slide_username=user.username)  # current user can't create slide is have same name already but another user can create same name
            slide_name = request.GET['slide_name']
            if not list_slide:
                slide = Slide(slide_name=slide_name, slide_username=user.username, slide_rst=request.GET['myTextBox'],
                              slide_share_time=timezone.now(), slide_last_update=timezone.now(), slide_rating=0,
                              slide_html_content=html_content, slide_theme=theme, slide_is_public=True)
                slide.save()
                return HttpResponseRedirect(reverse('rst2slides:presentationPage'))
            else:
                for slide in list_slide:
                    if slide_name == slide.slide_name:
                        if (theme == slide.slide_theme) and (request.GET['myTextBox'] == slide.slide_rst) and (
                                    slide.slide_is_public == True):
                            request.session['editSlide'] = "No Changes dectected"
                            return HttpResponseRedirect(reverse('rst2slides:editSlide', kwargs={'slide_name': slide.slide_name.replace(" ","_"),
                                                                                    'slide_username': slide.slide_username}))
                        else:
                            slide = Slide.objects.get(slide_name=slide_name)
                            slide.slide_rst = request.GET['myTextBox']
                            slide.slide_last_update = timezone.now()
                            slide.slide_html_content = html_content
                            slide.slide_theme = theme
                            slide.slide_is_public = True
                            slide.save()
                            return HttpResponseRedirect(reverse('rst2slides:presentationPage'))

                slide = Slide(slide_name=slide_name, slide_username=user.username,
                              slide_rst=request.GET['myTextBox'],
                              slide_share_time=timezone.now(), slide_last_update=timezone.now(), slide_rating=0,
                              slide_html_content=html_content, slide_theme=theme, slide_is_public=True)
                slide.save()
                return HttpResponseRedirect(reverse('rst2slides:presentationPage'))

        elif submit == 'private':
            old_slide_name = request.session['old_slide_name']
            slide = Slide.objects.get(slide_name=old_slide_name, slide_username=user.username)
            slide_name = request.GET['slide_name']
            list_slide = Slide.objects.filter(
                slide_name=slide_name)  # current user can't create slide is have same name already but another user can create same name

            for checkSlide in list_slide:
                if (slide_name != old_slide_name) and (
                            slide_name == checkSlide.slide_name):  # if change new name and new name have already. it can't create new.
                    request.session['editSlide'] = "New name is has already"
                    return HttpResponseRedirect(reverse('rst2slides:editSlide', kwargs={'slide_name': old_slide_name.replace(" ","_"),
                                                                                        'slide_username': slide.slide_username}))

            slide.slide_rst = request.GET['myTextBox']
            slide.slide_name = slide_name  # change the slide name
            slide.slide_last_update = timezone.now()
            slide.slide_html_content = html_content
            slide.slide_theme = theme
            slide.slide_is_public = False
            slide.save()
            return HttpResponseRedirect(reverse('rst2slides:userSlide'))

        elif submit == 'editPublic':
            old_slide_name = request.session['old_slide_name']
            slide = Slide.objects.get(slide_name=old_slide_name, slide_username=user.username)
            slide_name = request.GET['slide_name']
            list_slide = Slide.objects.filter(
                slide_name=slide_name)  # current user can't create slide is have same name already but another user can create same name

            for checkSlide in list_slide:
                if (slide_name != old_slide_name) and (
                            slide_name == checkSlide.slide_name):  # if change new name and new name have already. it can't create new.
                    request.session['editSlide'] = "New name is has already"
                    return HttpResponseRedirect(reverse('rst2slides:editSlide', kwargs={'slide_name': old_slide_name.replace(" ","_"),
                                                                                        'slide_username': slide.slide_username}))

            slide.slide_rst = request.GET['myTextBox']
            slide.slide_name = slide_name  # change the slide name
            slide.slide_last_update = timezone.now()
            slide.slide_html_content = html_content
            slide.slide_theme = theme
            slide.slide_is_public = True
            slide.save()
            return HttpResponseRedirect(reverse('rst2slides:userSlide'))


def uploadImagePage(request):
    request.session['imageName'] = ''  # clear image's name
    return render(request, 'rst2slides/uploadImagePage.html')


def uploadImage(request):
    if request.FILES != {}:  # if have file uploaded
        file = request.FILES['pic']
        user = request.user
        file_name = file.name.replace(" ", "")
        os.system('mkdir rst2slides/static/rst2slides/images/user/{}'.format(user.username))  # create directory folder
        dirPath = 'rst2slides/static/rst2slides/images/user/{}/'.format(user.username)
        saveFilePath = default_storage.save(dirPath, file)
        os.rename(saveFilePath, '{}{}'.format(dirPath, file_name))
        request.session['imageName'] = "{}/{}".format(user.username, file_name)
        return HttpResponseRedirect(reverse('rst2slides:imageCodeRst'))
    else:
        return HttpResponseRedirect(reverse('rst2slides:uploadImagePage'))

def imageCodeRst(request):
    return render(request, 'rst2slides/imageCodeRst.html')


def loginPage(request):
    request.session['home_page'] = ""
    request.session['editSlide'] = ""
    return render(request, 'rst2slides/loginPage.html')


def registerUser(request):
    username = request.POST['register name']
    email = request.POST['register email address']
    password = request.POST['register password']
    user_list = User.objects.all()
    for user in user_list:
        if username == user.username:
            request.session['loginPage'] = "{} is not Available".format(username)
            request.session['loginPageSuccess'] = ""
            return HttpResponseRedirect(reverse('rst2slides:loginPage'))
        elif email == user.email:
            request.session['loginPage'] = "{} is not Available".format(email)
            request.session['loginPageSuccess'] = ""
            return HttpResponseRedirect(reverse('rst2slides:loginPage'))

    user = User.objects.create_user(username, email, password)
    request.session['loginPageSuccess'] = "Register Successfully"
    request.session['loginPage'] = ""
    return HttpResponseRedirect(reverse('rst2slides:loginPage'))


def loginUser(request):
    user = authenticate(username=request.POST['username'], password=request.POST['userpassword'])
    if user is not None:
        # the password verified for the user
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('rst2slides:home_page'))
        else:
            request.session['loginPage'] = "Your account is disable"
            request.session['loginPageSuccess'] = ""
            return HttpResponseRedirect(reverse('rst2slides:loginPage'))
    else:
        # the authentication system was unable to verify the username and password
        request.session['loginPage'] = "username or password is incorrect"
        request.session['loginPageSuccess'] = ""
        return HttpResponseRedirect(reverse('rst2slides:loginPage'))


def presentationPage(request):
    request.session['loginPage'] = ""
    request.session['loginPageSuccess'] = ""
    request.session['home_page'] = ""
    request.session['editSlide'] = ""
    slides = Slide.objects.filter(slide_is_public=True)
    slide_url = []  # keep url_slide if slide have space if change to _
    for slide in slides:
        slide_url.append(slide.slide_name.replace(" ", "_"))
    zipped = zip(slides, slide_url)
    context = {'zipped': zipped}
    return render(request, 'rst2slides/presentationPage.html', context)


def contactPage(request):
    request.session['loginPage'] = ""
    request.session['loginPageSuccess'] = ""
    request.session['home_page'] = ""
    request.session['editSlide'] = ""
    return render(request, 'rst2slides/contactPage.html')


def logoutUser(request):
    logout(request)
    return HttpResponseRedirect(reverse('rst2slides:loginPage'))


def editSlide(request, slide_name, slide_username):
    slide = get_object_or_404(Slide, slide_name=slide_name.replace("_", " "), slide_username=slide_username)
    isCanEdit = slide_username == str(request.user)
    request.session['old_slide_name'] = slide.slide_name  # keep old slide name for change in ther future
    if isCanEdit  == False:
        slide.slide_rating = slide.slide_rating + 1  # another user only will increase views
    slide.save()
    last_theme = slide.slide_theme  # keep theme lastet
    all_theme = ['deck', 'clean', 'reveal','bootstrap']  # keep list of all theme
    context = {'slide': slide, 'isCanEdit': isCanEdit, 'last_theme':last_theme, 'all_theme':all_theme}
    return render(request, 'rst2slides/editSlide.html', context)


def watchSlide(request, slide_name, slide_username):
    slide = get_object_or_404(Slide, slide_name=slide_name.replace("_"," "), slide_username=slide_username)
    if slide_username != str(request.user):
        slide.slide_rating = slide.slide_rating + 1  # another user only will increase views
    slide.save()
    return HttpResponse(slide.slide_html_content)


def userSlide(request):
    request.session['loginPage'] = ""
    request.session['loginPageSuccess'] = ""
    request.session['home_page'] = ""
    request.session['editSlide'] = ""
    slides = Slide.objects.filter(slide_username=request.user.username)
    slide_url = []  # keep url_slide if slide have space if change to _
    for slide in slides:
        slide_url.append(slide.slide_name.replace(" ", "_"))
    zipped = zip(slides, slide_url)
    context = {'zipped': zipped}
    return render(request, 'rst2slides/userSlide.html', context)


def deleteSlide(request):
    slide_name = request.session['old_slide_name']
    user = request.user
    slide = get_object_or_404(Slide, slide_name=slide_name, slide_username=user.username)
    slide.delete()
    return HttpResponseRedirect(reverse('rst2slides:userSlide'))
