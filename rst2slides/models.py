from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Slide(models.Model):
    slide_name = models.CharField(max_length = 200)
    slide_username = models.CharField(max_length=200)
    slide_rst = models.TextField()
    slide_share_time = models.DateTimeField('date published')
    slide_last_update = models.DateTimeField('date last updated')
    slide_rating = models.IntegerField(default=0)
    slide_html_content = models.TextField(default="")
    slide_theme = models.CharField(max_length=200)
    slide_is_public = models.BooleanField(default=False)
    def __str__(self):
        return self.slide_name