from django.apps import AppConfig


class Rst2SlidesConfig(AppConfig):
    name = 'rst2slides'
