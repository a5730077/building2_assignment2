from django.conf.urls import url
from . import views

app_name = 'rst2slides'
urlpatterns = [
    url(r'^$',views.home_page,name =  'home_page'),
    url(r'^rst2html/$',views.rst2html, name = 'rst2html'),
    url(r'^uploadImagePage/$', views.uploadImagePage, name='uploadImagePage'),
    url(r'^uploadImage/$', views.uploadImage, name='uploadImage'),
    url(r'^imageCodeRst/$', views.imageCodeRst, name='imageCodeRst'),
    url(r'^loginPage/$', views.loginPage, name='loginPage'),
    url(r'^registerUser/$', views.registerUser, name='registerUser'),
    url(r'^loginUser/$', views.loginUser, name='loginUser'),
    url(r'^presentationPage/$', views.presentationPage, name='presentationPage'),
    url(r'^contactPage/$', views.contactPage, name='contactPage'),
    url(r'^logoutUser/$', views.logoutUser, name='logoutUser'),
    url(r'^(?P<slide_username>[a-zA-Z0-9_]*)/(?P<slide_name>[a-zA-Z0-9_]*)/editSlide/$', views.editSlide, name='editSlide'),
    url(r'^(?P<slide_username>[a-zA-Z0-9_]*)/(?P<slide_name>[a-zA-Z0-9_]*)/watchSlide/$', views.watchSlide, name='watchSlide'),
    url(r'^userSlide/$', views.userSlide, name='userSlide'),
    url(r'^deleteSlide/$', views.deleteSlide, name='deleteSlide'),
]